/**
 * kpiLogDetail.html 的js脚本
 * @author tieyq
 */
// table数据
var backData3 = {};
// 前 前期准备【几列】
var qqzbColspan;
// 中 节目制作保障【几列】
var jmzzbzColspan;
// 后 后期总结【几列】
var hqzjColspan;
// 总共【几列】
var totalColspan;
// 前 前期准备【分数】
var qqzbScore;
// 中 节目制作保障【分数】
var jmzzbzScore;
// 后 后期总结【分数】
var hqzjScore;
layui.use(['table', 'miniPage', 'util'], function () {
  var table = layui.table
      , $ = layui.$
      , miniPage = layui.miniPage
      , util = layui.util;


  var createOrDeleteKpiMemberUrl = iPAndPort + "/kpiLog/createOrDeleteKpiMember";
  var str = {
    "eventId": localStorage.getItem("kpiLogId")
  };

  // 1、获取活动下所有的人员，并进行CD操作
  $.ajax({
    async: false,
    type: "POST",
    url: createOrDeleteKpiMemberUrl,
    headers: {
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(str),
    dataType: "text",
    success: function (result) {

      var frameUrl = iPAndPort + "/kpiLog/getFrame";
      // 2、获取活动对应的考核信息
      $.ajax({
        async: false,
        type: "POST",
        url: frameUrl,
        headers: {
          'Content-Type': 'application/json'
        },
        data: JSON.stringify(str),
        dataType: "text",
        success: function (result) {
          var result = JSON.parse(result);
          // debugger;
          // 前 前期准备【几列】
          qqzbColspan = result.qqzbColspan;
          // 中 节目制作保障【几列】
          jmzzbzColspan = result.jmzzbzColspan;
          // 后 后期总结【几列】
          hqzjColspan = result.hqzjColspan;
          // 前 前期准备【分数】
          qqzbScore = result.qqzbScore;
          // 中 节目制作保障【分数】
          jmzzbzScore = result.jmzzbzScore;
          // 后 后期总结【分数】
          hqzjScore = result.hqzjScore;
          // 总共【几列】
          totalColspan = qqzbColspan + jmzzbzColspan + hqzjColspan;

          backData3 = JSON.parse(result.data);
          // console.log("------11--------")
          // console.log(backData3)
        },
        error: function (request) {
          // debugger;
          layer.msg("网络或服务异常，请稍后再试！");
        }
      });

    },
    error: function (request) {
      // debugger;
      layer.msg("网络或服务异常，请稍后再试！");
    }
  });


  var datas = []
  $.each(backData3, function (index, item) {
    var coll = {};
    for (key in item) {
      coll[key.split("#")[1]] = item[key];
    }
    datas.push(coll);
  })

  var colLists = [];

  colLists = [];
  var colls = [];
  const site = 0;
  // 顺序号+中文名
  var colList = {};
  // 顺序号+字段名
  var colFieldList = {};
  for (key in backData3[0]) {
    colList[key.split("#")[0]] = key.split("#")[2];
    colFieldList[key.split("#")[0]] = key.split("#")[1];
  }

  // 仅这几列，允许占用两行
  var arrStr = ["id", "序号", "姓名", "小计", "备注"];
  for (key in colList) {
    // 非标准字段，跳过
    if (colList[key] == null || colList[key] == 'undefined') {
      continue;
    }
    var coll = {};
    if ("姓名" == colList[key]) {
      // coll['width'] = "80";
      coll['totalRowText'] = "合计";
    }else{
      coll['totalRow'] = true;

    }
    // 列宽自适应
    var autoColspan = (1/(totalColspan+3)) * 100;
    coll['width'] = autoColspan + "%";

    // debugger;
    if (colList[key].includes('前期准备')) {
      // console.log('----------- 进入 前期准备 -----------')
      coll['field'] = colFieldList[key];
      coll['edit'] = 'text';
      coll['title'] = colList[key].split("-")[1];
      coll['align'] = 'center';
      colls.push(coll);
      // colLists.push(coll);
    } else if (colList[key].includes('节目制作保障')) {
      // console.log('----------- 进入 节目制作保障 -----------')
      // console.log(colList[key])
      coll['field'] = colFieldList[key];
      coll['edit'] = 'text';
      coll['title'] = colList[key].split("-")[1];
      coll['align'] = 'center';
      colls.push(coll);

    } else if (colList[key].includes('后期总结')) {
      // console.log('----------- 进入 后期总结 -----------')
      coll['field'] = colFieldList[key];
      coll['edit'] = 'text';
      coll['title'] = colList[key].split("-")[1];
      coll['align'] = 'center';
      colls.push(coll);
    }

    if (true) {
      // 姓名后  第一阶段
      if (colList[key].includes('姓名')) {
        coll['field'] = colFieldList[key];
        coll['title'] = colList[key];
        coll['rowspan'] = 2;
        coll['align'] = 'center';
        colLists.push(coll);

        var colll = {};
        colll['field'] = 'qqzb';
        colll['title'] = '前期准备(' + qqzbScore + ')';
        colll['align'] = 'center';
        // 动态列
        colll['colspan'] = qqzbColspan;
        colLists.push(colll);

        continue;
      }

      // 设备整理装车后  第二阶段
      if (colList[key].includes('设备整理装车')) {
        var colll = {};
        colll['field'] = 'jmzzbz';
        colll['title'] = '节目制作保障(' + jmzzbzScore + ')';
        colll['align'] = 'center';
        // 动态列
        colll['colspan'] = jmzzbzColspan;
        colLists.push(colll);
        continue;
      }

      // 后勤后  第三阶段
      if (colList[key].includes('后勤')) {
        var colll = {};
        colll['field'] = 'hqzj';
        colll['title'] = '后期总结(' + hqzjScore + ')';
        colll['align'] = 'center';
        // 动态列
        colll['colspan'] = hqzjColspan;
        colLists.push(colll);
        continue;
      }

      // 仅这几列【"id", "序号", "姓名", "小计", "备注"】，允许占用两行
      if (arrStr.includes(colList[key])) {
        var colll = {};
        colll['field'] = colFieldList[key];
        colll['title'] = colList[key];
        colll['edit'] = 'text';
        // id隐藏
        if ("id" == colList[key]) {
          colll['hide'] = true;
        }
        // 序号递增
        if ("序号" == colList[key]) {
          colll['templet'] = '#orderNumber';
          colll['edit'] = '';
        }
        // 小计
        if ("小计" == colList[key]) {
          colll['totalRow'] = true;
          colll['edit'] = '';
          colll['templet'] =function(d){
               var result = Number(
                   nullToZero(d.ywdj) + nullToZero(d.jstc) + nullToZero(d.sbzlzc) +
                         nullToZero(d.xtdj) + nullToZero(d.db) + nullToZero(d.sx) + nullToZero(d.zm) + nullToZero(d.wljs) + nullToZero(d.jk) + nullToZero(d.yp) + nullToZero(d.spzz) + nullToZero(d.evs) + nullToZero(d.dg) + nullToZero(d.lzcs) + nullToZero(d.hqjsy) +
                         nullToZero(d.sbrk) + nullToZero(d.zlwh) + nullToZero(d.fpzj)
               ).toFixed(2);
              return result;
            }
        }
        // 备注
        if ("备注" == colList[key]) {
          colll['width'] = "100";
        }
        colll['rowspan'] = 2;
        colll['align'] = 'center';
        colLists.push(colll);
      }

    }
  }
  // console.log('----------- colLists -----------')
  // console.log('colLists', colLists)
  // 渲染
  table.render({
    elem: '#kpiLogDetailTableId'
    , data: datas
    , cols: [colLists, colls]
    , totalRow: true
    , page: false

  });
  //监听单元格编辑
  table.on('edit(kpiLogDetailTableFilter)', function (obj) {
    var value = obj.value //得到修改后的值
        , data = obj.data //得到所在行所有键值
        , field = obj.field; //得到字段

    var dataUrl = iPAndPort + "/kpiLog/updateById";
    // 一边编辑、一边保存 (按行保存)
    $.ajax({
      async: false,
      type: "POST",
      url: dataUrl,
      headers: {
        'Content-Type': 'application/json'
      },
      data: JSON.stringify(data),
      dataType: "json",
      success: function (result) {
        // 字段名
        var title="";
        switch (field){
          case 'qqzb':title='前期准备';break;
          case 'jmzzbz':title='节目制作保障';break;
          case 'hqzj':title='后期总结';break;
          case 'ywdj':title='业务对接';break;
          case 'jstc':title='技术统筹';break;
          case 'sbzlzc':title='设备整理装车';break;
          case 'xtdj':title='系统搭建';break;
          case 'db':title='导播';break;
          case 'sx':title='摄像';break;
          case 'zm':title='字幕';break;
          case 'wljs':title='网络技术';break;
          case 'jk':title='技控';break;
          case 'yp':title='音频';break;
          case 'spzz':title='视频制作';break;
          case 'evs':title='EVS回放';break;
          case 'dg':title='灯光';break;
          case 'lzcs':title='录制、传输';break;
          case 'hqjsy':title='后勤、驾驶员';break;
          case 'sbrk':title='设备入库';break;
          case 'zlwh':title='整理维护';break;
          case 'fpzj':title='复盘总结';break;
          case 'xj':title='小计';break;
          default:title=field;
        }
        if(value == null || value == ''){
          value = 0;
        }else{
          value = util.escape(value);
        }
        if(title != 'remark'){
          layer.msg('[' + data.ry + '] ' + title + '得分为：' + value)
        }
        //执行搜索重载
        table.reload('kpiLogDetailTableId', {
        }, 'data');
      },
      error: function (request) {
        debugger;
        layer.msg("操作失败！");
      }
    });
  });

  // 打开新页面->分数划分
  document.getElementById("planKpiBtn").onclick = function () {
    // localStorage.setItem("kpiLogId",data.id);
    var content = miniPage.getHrefContent('page/kpi/kpiPlan.html');
    var openWH = miniPage.getOpenWidthHeight();

    var index = layer.open({
      title: '分数划分',
      type: 1,
      shade: 0.2,
      maxmin: true,
      shadeClose: true,
      area: [openWH[0] + 'px', openWH[1] + 'px'],
      offset: [openWH[2] + 'px', openWH[3] + 'px'],
      content: content,
      // end: function () {
      //   table.reload('kpiLogDetailTableId', {
      //     // page: {
      //     //   curr: 1
      //     // }
      //     // , where: null
      //   }, 'data');
      // }
    });
    $(window).on("resize", function () {
      layer.full(index);
    });
    return false;
  }

  // null值处理的function
  function nullToZero(param) {
    if (param === null || param === undefined || param === '') {
      return Number(0);
    }
    return Number(param);
  }
});
